<?php

include('../vendor/autoload.php');

use App\User;

if($_SERVER['REQUEST_METHOD']=='POST') {

    
    $password = trim($_POST["password"]);
    $email = trim($_POST["email"]);


    if($password == "" || $email == ""){

        if($password == ""){
            $error['password']="Required";
        }
        if($email == ""){
            $error['email']="Required";
        }

    }else {
        
        if ($user = User::login('users',$email,$password)) {
            session_start();
            $_SESSION['user'] = $user;
            header('location:products.php');
        } else {
            header('location:login.php?email='.$email);
        }
    }

}
    
    include_once('./views/login.php');
