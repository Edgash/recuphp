<?php
include('../vendor/autoload.php');

use App\Product;
use App\Module;


$modulesNombres = Module::getAll();

$id = $_GET["id"];


$book = Product::getProductById($id);

$fotoLibro = $book->photo;



if(isset($_POST["publisher"]) && ($_POST["price"]) && ($_POST["pages"]) ) {

if ($_SERVER['REQUEST_METHOD'] === 'POST'){

    if ($_FILES['photo']['size'] == 0){
       
        $ruta = $fotoLibro;

        
    }else{

        $file = $_FILES["photo"]["name"]; 

        $url_temp = $_FILES["photo"]["tmp_name"]; 

        $url_insert = dirname(__FILE__) . "/images"; 

        $url_target = str_replace('\\', '/', $url_insert) . '/' . $file;

        $ruta = "./images/".$file;

        
    }
   
    $userlibro = Product::update(['idModule'=>$_POST['module'],'publisher'=>$_POST['publisher'],'price'=>$_POST['price'],'pages'=>$_POST['pages'],
                                    'status'=>$_POST['status'],'photo'=>$ruta,'comments'=>$_POST['comments']], $id);
    header("Location: product.php?id=".$id);
    
}

}else{
    include_once('./views/updateProduct.php');
}