<html>
    <head>
        <title>Edit Product</title>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD"
            crossorigin="anonymous"
        >
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"
            crossorigin="anonymous"
        >
        </script>
    </head>
    <body>
    <section class="col-md-12">
        <div class="container  d-flex justify-content-center">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="display-4">Edit Product</h1>
                </div>
            </div>
            <div class="row">
                <form method="post" action="updateProduct.php?id=<?=$book->id?>" enctype="multipart/form-data" >
                    <!-- Publisher input -->
                    <div class="form-outline mb-4">
                        <input type="text" name="publisher" id="formRegister1" class="form-control" value="<?=$book->publisher?>"/>
                        <label class="form-label" for="formRegister1">Publisher</label>
                    </div>
                    <!-- Price input -->
                    <div class="form-outline mb-4">
                        <input type="number" name="price" id="formRegister2" class="form-control" value="<?=$book->price?>"/>
                        <label class="form-label" for="formRegister2">Price</label>
                    </div>
                    <!-- Pages input -->
                    <div class="form-outline mb-4">
                        <input type="number" name="pages" id="formRegister3" class="form-control" value="<?=$book->pages?>"/>
                        <label class="form-label" for="formRegister3">Pages</label>
                    </div>
                    <!-- Status input -->
                    <div class="form-outline mb-4">
                        <select name="status">   
                            <option value="new" <?= ($book->status === "new")?'selected':'' ?>>New</option>
                            <option value="good" <?= ($book->status === "good")?'selected':'' ?>>Good</option>
                            <option value="used" <?= ($book->status === "used")?'selected':'' ?>>Used</option>
                            <option value="bad" <?= ($book->status === "bad")?'selected':'' ?>>Bad</option>
                        </select>
                        <br>
                        <label class="form-label" for="formRegister3">Status</label>
                    </div>
                    <!-- Photo input -->
                    <div class="form-outline mb-4">
                    <input id="photo" name="photo" type="file" value="<?=$book->photo?>">
                        <br>
                        <label class="form-label" for="formRegister3">Photo</label>
                    </div>
                    <!-- Comments input -->
                    <div class="form-outline mb-4">
                        <input type="text" name="comments" id="formRegister6" class="form-control" value="<?=$book->comments?>"/>
                        <label class="form-label" for="formRegister3">Comments</label>
                    </div>
                    <!-- Module input -->
                    <div class="form-outline mb-4">
                        <select id="module" name="module">   
                        <?php   
                            foreach ($modulesNombres as $modulos) { ?>
                                <option value="<?= $modulos->code ?>" <?= ($modulos->code === $book->idModule)?'selected':'' ?>><?=$modulos->cliteral?></option>
                        <?    
                            }
                        ?>
                        </select>
                        <br>
                        <label class="form-label" for="formRegister3">Module</label>
                    </div>
                    <!-- Submit button -->
                    <button type="submit" class="btn btn-primary btn-block">Edit book</button>
                </form>
            </div>
        </div>
    </section>
    </body>
</html>