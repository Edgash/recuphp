<?php
include('../vendor/autoload.php');

use App\Connection;

if(isset($_POST["nick"]) && ($_POST["email"]) && ($_POST["password"]) ) {

    $connection = new Connection();
   
    $nick = htmlspecialchars(trim($_POST["nick"]));
    $password = trim($_POST["password"]);
    $email = trim($_POST["email"]);


    if($nick == "" || $password == "" || $email == ""){

        echo '<script>
                alert("Error!\ Algún campo no está rellenado");
              </script>';

    }else {
        if ($connection->insert('users',['nick'=>$nick,'email'=>$email,'password'=>password_hash($password,PASSWORD_DEFAULT)])) {
            session_start();
            $_SESSION['nick'] = $nick;
            $_SESSION['password'] = $password;
        }
        
        header("Location: ./views/index.php");
    }


}else{

include_once('./views/register.php');
}