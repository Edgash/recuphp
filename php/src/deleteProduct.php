<?php

include('../vendor/autoload.php');

use App\Product;


$id = $_GET["id"];
$product = Product::delete($id);

header('Location: ./products.php');