<?php
namespace App;

use App\Connection;
use PDO;
use PDOException;

class User{

    public $email;
    private $password;

    public static function login($tabla, $email, $password){

        $connec = new Connection();
        $connection = $connec->mbd;

        $sql = "SELECT * FROM $tabla WHERE email=:email";
        

        $sentencia = $connection -> prepare($sql);
        $sentencia->bindValue(':email',$email);
        $sentencia -> setFetchMode(PDO::FETCH_CLASS,'App\User');
        $res = $sentencia -> execute();

        $user = $sentencia->fetch();
        
        if ($user) {
            if (password_verify($password, $user->password)) {
                return $user;
            } else {
                return false;
            }
        }    

    }


    public static function getAll() {
        
        
        $connec = new Connection();
        $connection = $connec->mbd;

        $sql = "SELECT * FROM user";
        

        $sentencia = $connection -> prepare($sql);
        $sentencia -> setFetchMode(PDO::FETCH_CLASS,'App\User');
        $res = $sentencia -> execute();

        $user = $sentencia->fetchAll();
        
        return $user;

    }

    public static function getProductById($id) {
        
        
        $connec = new Connection();
        $connection = $connec->mbd;

        $sql = "SELECT * FROM user WHERE id = ".$id;
        

        $sentencia = $connection -> prepare($sql);
        $sentencia -> setFetchMode(PDO::FETCH_CLASS,'App\User');
        $res = $sentencia -> execute();

        $user = $sentencia->fetch();
        
        return $user;

    }
}