<?php
namespace App;

use PDO;
use PDOException;

include_once($_SERVER['DOCUMENT_ROOT']."/config/conf.php");

class Connection{

    public $mbd;

    public function __construct() {
        try {
            $this->mbd = new PDO(DSN, USUARIO, PASSWORD);
            $this->mbd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Falló la conexión: ' . $e->getMessage();
        }
    }


    public function insert($tabla, $valores){
        $keys = array_keys($valores);
        
        $sql = "INSERT INTO $tabla (".implode(', ',$keys).") VALUES (:".implode(', :',$keys).")";
    
        
        try {
            $sentencia = $this->mbd->prepare($sql);
            
            foreach ($valores as $key => $value) {
                $sentencia->bindValue(":$key", $value);
            }
            
            $sentencia->execute();
        } catch (PDO_EXCEPTION $exception) {
            echo $exception->getMessage();
            die();
        }
        return true;


    }
}