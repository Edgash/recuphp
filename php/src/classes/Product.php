<?php
namespace App;

use App\Connection;
use PDO;
use PDOException;

class Product{
    
    public static function getAll() {
        
        
        $connec = new Connection();
        $connection = $connec->mbd;

        $sql = "SELECT * FROM books";
        

        $sentencia = $connection -> prepare($sql);
        $sentencia -> setFetchMode(PDO::FETCH_CLASS,'App\Product');
        $res = $sentencia -> execute();

        $book = $sentencia->fetchAll();
        
        return $book;

    }

    public static function insert($valores){
        $connec = new Connection();
        
        $connec->insert('books',$valores);
    }

    public static function getProductById($id) {
        
        
        $connec = new Connection();
        $connection = $connec->mbd;

        $sql = "SELECT * FROM books WHERE id = ".$id;
        

        $sentencia = $connection -> prepare($sql);
        $sentencia -> setFetchMode(PDO::FETCH_CLASS,'App\Product');
        $res = $sentencia -> execute();

        $book = $sentencia->fetch();
        
        return $book;

    }

    public function nombreModule(){

        $connec = new Connection();
        $connection = $connec->mbd;

        $sql = "SELECT cliteral FROM modules WHERE code = :code";
        

        $sentencia = $connection -> prepare($sql);
        $sentencia->bindValue(":code",$this->idModule);
        $sentencia -> setFetchMode(PDO::FETCH_CLASS,'App\Product');
        
        $res = $sentencia -> execute();

        $module = $sentencia->fetch();
    
        return $module->cliteral;

    }

    public static function delete($idBook){

        $connec = new Connection();
        $connection = $connec->mbd;

        $sql = "DELETE FROM `books` WHERE id = ".$idBook;
        
        $sentencia = $connection -> prepare($sql);
        $sentencia -> setFetchMode(PDO::FETCH_CLASS,'App\Product');
        
        $res = $sentencia -> execute();

        $del = $sentencia->fetch();

    }

    public static function update($valores, $id){

        $connec = new Connection();
        $connection = $connec->mbd;

        $keys = array_keys($valores);
        
        $sql = "UPDATE books SET ";
        foreach ($keys as $key){
            $sql .= $key.'= :'.$key.',';
        }
        $sql = trim($sql,',');
        $sql .= " WHERE id =".$id;
        
        try {
            $sentencia = $connection->prepare($sql);
            
            foreach ($valores as $key => $value) {
                $sentencia->bindValue(":$key", $value);
            }
            
            $sentencia->execute();

            $book = $sentencia->fetch();
        } catch (PDO_EXCEPTION $exception) {
            echo $exception->getMessage();
            die();
        }
        return $book;

    }
   
}