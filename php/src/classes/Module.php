<?php
namespace App;

use App\Connection;
use PDO;
use PDOException;

class Module{
    
    public static function getAll() {
        
        
        $connec = new Connection();
        $connection = $connec->mbd;

        $sql = "SELECT * FROM modules";
        

        $sentencia = $connection -> prepare($sql);
        $sentencia -> setFetchMode(PDO::FETCH_CLASS,'App\Module');
        $res = $sentencia -> execute();

        $module = $sentencia->fetchAll();
        
        return $module;

    }


    public static function getProductById($id) {
    
        $connec = new Connection();
        $connection = $connec->mbd;

        $sql = "SELECT * FROM module WHERE id = ".$id;
        

        $sentencia = $connection -> prepare($sql);
        $sentencia -> setFetchMode(PDO::FETCH_CLASS,'App\Module');
        $res = $sentencia -> execute();

        $module = $sentencia->fetch();
        
        return $module;

    }
}