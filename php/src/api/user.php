<?php
include($_SERVER['DOCUMENT_ROOT'].'/../vendor/autoload.php');

use App\User;
use App\Connection;


$connec = new Connection();
$connection = $connec->mbd;


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['user'])){
        showJson(User::getProductById($_GET['user']));
    } else {
        showJson(User::getAll());
    }
} else {
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){

        $nick = trim($_POST["nick"]);
        $email = trim($_POST["email"])
        $password = trim($_POST["password"]);

        Connection::insert('users',['nick'=>$nick,'email'=>$email,'password'=>password_hash($password,PASSWORD_DEFAULT)]);
    }
}


function showJson($result) {
    header('Content-Type: application/json');
    echo json_encode($result);  
}




?>