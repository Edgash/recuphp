<?php
include($_SERVER['DOCUMENT_ROOT'].'/../vendor/autoload.php');

use App\Product;
use App\Connection;


$connec = new Connection();
$connection = $connec->mbd;
    

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['book'])){
        showJson(Product::getProductById($_GET['book']));
    } else {
        showJson(Product::getAll());
    }
} else {
    if ($_SERVER['REQUEST_METHOD'] == 'POST'){
        $publisher = trim($_POST["publisher"]);
        $price = $_POST["price"];
        $pages = $_POST["pages"];
        $status = $_POST["status"];
        $idUser = $_POST['idUser'];
        $comments = trim($_POST["comments"]);
        $idModule = ($_POST["module"]);

        $file = $_FILES["photo"]["name"]; 
        $url_temp = $_FILES["photo"]["tmp_name"]; 
        $url_insert = dirname(__FILE__) . "/images"; 
        $url_target = str_replace('\\', '/', $url_insert) . '/' . $file;
        $ruta = "./images/".$file;

        Product::insert(['idUser'=>$idUser,'idModule'=>$idModule,'publisher'=>$publisher,'price'=>$price,'pages'=>$pages,'status'=>$status,'photo'=>$ruta,'comments'=>$comments]);
    }
}


function showJson($result) {
    header('Content-Type: application/json');
    echo json_encode($result);  
}




?>