<?php
include($_SERVER['DOCUMENT_ROOT'].'/../vendor/autoload.php');

use App\Module;
use App\Connection;


$connec = new Connection();
$connection = $connec->mbd;


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (isset($_GET['modules'])){
        showJson(Module::getProductById($_GET['modules']));
    } else {
        showJson(Module::getAll());
    }
} 

function showJson($result) {
    header('Content-Type: application/json');
    echo json_encode($result);  
}


?>