<?php
include('../vendor/autoload.php');

use App\User;
session_start();

if (!isset($_SESSION['user'])) {
    header('location:login.php');
}

echo "Benvigut ".$_SESSION['user']->nick;
