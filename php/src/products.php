<?php

include('../vendor/autoload.php');

use App\Product;


$products = Product::getAll();

session_start();

if (!isset($_SESSION['user'])) {
    header('location:login.php');
}

$sesion = $_SESSION['user']->id;


$registrosPorPagina = 3;

$totalRegistros = count($products);

$totalPaginas = ceil($totalRegistros / $registrosPorPagina);

$paginaActual = isset($_GET['pagina']) ? (int)$_GET['pagina'] : 1;

$indiceInicio = ($paginaActual - 1) * $registrosPorPagina;
$indiceFin = $indiceInicio + $registrosPorPagina - 1;

$registrosParaPaginaActual = array_slice($products, $indiceInicio, $registrosPorPagina);


include_once('./views/products.php');