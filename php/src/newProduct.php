<?php
include('../vendor/autoload.php');

use App\Connection;
use App\Module;
use App\Product;

session_start();
if (!isset($_SESSION['user'])) {
    header('location:login.php');
}


$modulesNombres = Module::getAll();


if(isset($_POST["publisher"]) && ($_POST["price"]) && ($_POST["pages"]) ) {

    

    $sesion = $_SESSION['user']->id;
    $publisher = trim($_POST["publisher"]);
    $price = $_POST["price"];
    $pages = $_POST["pages"];
    $status = $_POST["status"];
    
    $comments = trim($_POST["comments"]);
    $idModule = ($_POST["module"]);
    
    if($publisher == "" || $price == "" || $pages == ""){

    }else {
        
        $file = $_FILES["photo"]["name"]; 

        $url_temp = $_FILES["photo"]["tmp_name"]; 

        $url_insert = dirname(__FILE__) . "/images"; 

        $url_target = str_replace('\\', '/', $url_insert) . '/' . $file;

        $ruta = "./images/".$file;

        Product::insert(['idUser'=>$sesion,'idModule'=>$idModule,'publisher'=>$publisher,'price'=>$price,'pages'=>$pages,'status'=>$status,'photo'=>$ruta,'comments'=>$comments]);
        
        
        header("Location: ./products.php");

    }


}else{

include_once('./views/newProduct.php');
}